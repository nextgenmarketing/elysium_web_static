import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Feed, PostModel } from './const';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss']
})
export class FeedsComponent implements OnInit {
  postControl = new FormControl();
  feedData: PostModel[] = Feed;

  constructor() { }

  ngOnInit(): void {}

  post() {}
}
