export const Feed: PostModel[]  = [
    {
        profilePhoto: 'assets/malenia.PNG',
        name: 'Malenia',
        date: new Date(),
        postText: 'I am Malenia,  Blade of Miquella',
        postPhoto: 'assets/hand.PNG',
        likeCount: '123'
    },
    {
        profilePhoto: 'assets/ranni.PNG',
        name: 'Ranni',
        date: new Date(),
        postText: 'Keep it quiet Overflow OverflowOverflow OverflowOverflow Overflow OverflowOverflow OverflowOverflow Overflow OverflowOverflow OverflowOverflow Overflow OverflowOverflow OverflowOverflow',
        postPhoto: 'assets/ranni_meme.jpg',
        likeCount: '69'
    },
    {
        profilePhoto: 'assets/marika.PNG',
        name: 'Marika',
        date: new Date(),
        postText: 'Who am I',
        postPhoto: '',
        likeCount: '69'
    },
    {
        profilePhoto: 'assets/melina.PNG',
        name: 'Melina',
        date: new Date(),
        postText: 'Burn it down',
        postPhoto: '',
        likeCount: '69'
    },
    {
        profilePhoto: 'assets/radagon.PNG',
        name: 'Radagon',
        date: new Date(),
        postText: 'I am who',
        postPhoto: '',
        likeCount: '69'
    },
];

export interface PostModel {
    profilePhoto: string;
    name: string;
    date: Date;
    postText: string;
    postPhoto: string;
    likeCount: string;
}