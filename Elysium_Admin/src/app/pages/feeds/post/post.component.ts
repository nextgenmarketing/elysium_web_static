import { Component, Input, OnInit } from '@angular/core';
import { PostModel } from '../const';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() post: PostModel | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
