import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { UserTypes } from '../communications/model';
import { SaleTypes, TrashData } from  './model';

@Component({
  selector: 'app-marketing',
  templateUrl: './marketing.component.html',
  styleUrls: ['./marketing.component.scss']
})
export class MarketingComponent implements OnInit {
  displayedColumns = ['type', 'userlist', 'products', 'sendDate', 'message'];
  tableData = TrashData;
  userTypes = UserTypes;
  saleTypes = SaleTypes;
  sendLater = false;
  sendFeed = false;
  products = [
    {id: 1, value: 'Product 1'},
    {id: 1, value: 'Product 2'}
  ];

  saleControl = new FormControl();
  userControl = new FormControl();
  productControl = new FormControl();
  timeControl = new FormControl();
  messControl = new FormControl();

  constructor() { }

  ngOnInit(): void {
  }

  add() {

  }

}
