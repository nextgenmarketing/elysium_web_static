const L = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'
];

export interface GridItem {
    id: number;
    type: string[];
    message: string;
    userlist: string[];
    sendDate: Date;
};

export const TrashData = [
    {
        id: 0,
        type: ['Email'],
        message: L[0],
        sendDate: new Date(),
        userList: ['Follower']
    },
    {
        id: 1,
        type: ['Text'],
        message: L[1],
        sendDate: new Date(),
        userList: ['Subscriber']
    },
    {
        id: 2,
        type: ['Push'],
        message: L[2],
        sendDate: new Date(),
        userList: ['Other']
    },
    {
        id: 3,
        type: ['Email', 'Text'],
        message: L[0],
        sendDate: new Date(),
        userList: ['Follower', 'Subscriber']
    },
    {
        id: 4,
        type: ['Text', 'Push'],
        message: L[1],
        sendDate: new Date(),
        userList: ['Subscriber', 'Other']
    },
    {
        id: 5,
        type: ['Push', 'Email'],
        message: L[2],
        sendDate: new Date(),
        userList: ['Other', 'Follower']
    },
    {
        id: 6,
        type: ['Email', 'Push'],
        message: L[0],
        sendDate: new Date(),
        userList: ['Follower', 'Other']
    },
    {
        id: 7,
        type: ['Text', 'Email'],
        message: L[1],
        sendDate: new Date(),
        userList: ['Subscriber', 'Follower']
    },
    {
        id: 8,
        type: ['Push', 'Email'],
        message: L[2],
        sendDate: new Date(),
        userList: ['Other', 'Subscriber']
    },
    {
        id: 9,
        type: ['Email', 'Text', 'Push'],
        message: L[0],
        sendDate: new Date(),
        userList: ['Follower', 'Subscriber', 'Other']
    },
    {
        id: 10,
        type: ['Text', 'Push', 'Email'],
        message: L[1],
        sendDate: new Date(),
        userList: ['Subscriber', 'Other', 'Follower']
    },
    {
        id: 11,
        type: ['Push', 'Text', 'Email'],
        message: L[2],
        sendDate: new Date(),
        userList: ['Other', 'Subscriber', 'Follower']
    }
];

export const CommunicationTypes = [
    {id: 1, value: 'Email'},
    {id: 2, value: 'Push Notification'},
    {id: 3, value: 'Text'},
]

export const UserTypes = [
    {id: 1, value: 'Subscriber'},
    {id: 2, value: 'Follower'},
    {id: 3, value: 'General'},
]