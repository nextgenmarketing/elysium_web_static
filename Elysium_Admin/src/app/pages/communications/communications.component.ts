import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

// import { MatPaginator } from '@angular/material/paginator';

import { CommunicationTypes, TrashData, UserTypes } from './model';


@Component({
  selector: 'app-communications',
  templateUrl: './communications.component.html',
  styleUrls: ['./communications.component.scss'],
})

export class CommunicationsComponent implements OnInit {
  displayedColumns = ['type', 'userlist', 'sendDate', 'message'];
  tableData: any [] = [];
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  commTypes = CommunicationTypes;
  userTypes = UserTypes;
  sendLater = false;
  sendFeed = false;

  typeControl = new FormControl();
  messControl = new FormControl();
  userControl = new FormControl();
  timeControl = new FormControl();

  constructor() {}

  ngOnInit(): void {
    //api request to get grid data
    this.tableData = TrashData;
  }
  add(){
    const createModel = {
      type: this.typeControl.value,
      message: this.messControl.value,
      users: this.typeControl.value,
      date: this.timeControl.value,
    };
    //api request to add to grid
    if (this.sendFeed) {
      // api request to add to feed
    }
  }
}
