export const Profile = {
    type: ['Influencer', 'Marketer', 'Influencer', 'Affiliate', 'Business Owner'],
    name: 'Big Pimpin LLC',
    accountType: 'Store', // Corporate/ Store
    bio: 'This my store',
    photo: 'assets/malenia.PNG',
    address: '420 Swag lane',
    email: 'bbcgang@gmail.com',
    paymentInformation: {
        type: 'Debit Card',
        obfiData: '************1232'
    },
    products: [
        
    ],
    // add services to data model document
    services: [

    ],
    sales: [
        
    ],
    newsletter: true,
    affiliateMarketing: true,
    // affiliateMarketers: [{},],
    //affiliateSales: [{},],
    // password: '',
    followers: 123,
    subscribers: 123,
    views: 123,
    clickThroughRate: 123,
    conversionRate: 123,
    contentReleaseRate: 123,
    numberOfPictureScans: 123,
    referralCount: 123,
    associatedAccounts: [
        123,
        123
    ],
    productScanMatch: [], // wtf is this
};

export interface Product {
    // type: string; // Service or Product // no product is a product and service is a service
    // saleTypes: object, //(Coupon,Promotion,Subscription, Affiliate, Limited Time Sales)
    // assosiatedAccounts: []; // like who owns it? no need
    name: string,
    category: string; // like the category you would find it under on amazon
    description: string,
    price: number,
    brandPointValue: number;
    loyaltyPoints: number;
    quantityAvailable: number,
    deliveryAvailability: boolean;
    pickupAvailability: boolean;
    views: number;
    conversionRate: number;
}

export interface Service {
    // type: string[];
    // serviceCategory: []; redundant
    // assosiatedAccounts: [];
    name: string;
    category: string; // like the category you would find it under on amazon
    description: string;
    price: number;
    brandPointValue: number;
    loyaltyPoints: number;
    durationOfService: number;
    availabilityDuration: number;
    disclaimer: string;
    views: number;
    conversionRate: number;
}
