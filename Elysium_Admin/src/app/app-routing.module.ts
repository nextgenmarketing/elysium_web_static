import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  AnalyticsComponent,
  CommunicationsComponent,
  FeedsComponent,
  HelpComponent,
  HomeComponent,
  MarketingComponent,
  NotFoundComponent,
  OfferingsComponent,
  ProfileComponent,
} from './pages';
import { ForgotComponent } from './signIn/forgot/forgot.component';
import { AuthGuard } from './signIn/services/auth.guard';
import { SignInComponent } from './signIn/sign-in/sign-in.component';
import { SignUpComponent } from './signIn/sign-up/sign-up.component';
import { VerifyComponent } from './signIn/verify/verify.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'sign-in',
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'analytics',
    component: AnalyticsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'communications',
    component: CommunicationsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'feeds',
    component: FeedsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'marketing',
    component: MarketingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'offerings',
    component: OfferingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'help',
    component: HelpComponent,
  },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'forgot-password', component: ForgotComponent },
  { path: 'verify-email-address', component: VerifyComponent },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
