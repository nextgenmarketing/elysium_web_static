import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AnalyticsComponent } from './pages/analytics/analytics.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OfferingsComponent } from './pages/offerings/offerings.component';
import { HelpComponent } from './pages/help/help.component';
import { FeedsComponent } from './pages/feeds/feeds.component';
import { CommunicationsComponent } from './pages/communications/communications.component';
import { MarketingComponent } from './pages/marketing/marketing.component';

import { MaterialModule } from './material.module';
import { PostComponent } from './pages/feeds/post/post.component';
import { environment } from '../environments/environment';
import { AuthService } from './signIn/services/auth.service';
import { SignInComponent } from './signIn/sign-in/sign-in.component';
import { SignUpComponent } from './signIn/sign-up/sign-up.component';
import { ForgotComponent } from './signIn/forgot/forgot.component';
import { VerifyComponent } from './signIn/verify/verify.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    AnalyticsComponent,
    NotFoundComponent,
    OfferingsComponent,
    HelpComponent,
    FeedsComponent,
    CommunicationsComponent,
    MarketingComponent,
    PostComponent,
    SignInComponent,
    SignUpComponent,
    ForgotComponent,
    VerifyComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
  ],
  providers: [
    AuthService
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
